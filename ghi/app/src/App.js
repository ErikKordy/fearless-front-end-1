import Nav from './Nav';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage'

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <BrowserRouter>
    <Nav />
    <Routes>
      <Route index element={<MainPage />}></Route>
      <Route path="attendees">
          <Route index element={<AttendeesList attendees={props.attendees} />}></Route>
          <Route path="new" element={<AttendConferenceForm />}></Route>
        </Route>
        <Route path="locations/new" element={<LocationForm />}></Route>
        <Route path="conferences/new" element={<ConferenceForm />}></Route>
        <Route path="presentation/new" element={<PresentationForm />}></Route>
      </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
