import React, {useEffect, useState} from 'react'

function ConferenceForm() {


    const handleSubmit = async (event) => {
        event.preventDefault();
      
        const data = {};

        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees
        data.description = description
        data.location = location
        console.log(data);
      
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
      
    const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setStartDate('');
          setEndDate('');
          setMaxPresentations('');
          setMaxAttendees('');
          setDescription('');
        }
      }

    const [name, setName] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [startDate, setStartDate] = useState('')

    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value)
    }

    const [endDate, setEndDate] = useState('')

    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value)
    }

    const [maxPresentations, setMaxPresentations] = useState('')
    
    const handlePresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value)
    }

    const [maxAttendees, setMaxAttendees] = useState('')

    const handleAttendeesChange = (event) => {
        const value = event.target.value
        setMaxAttendees(value)
    }

    const [description, setDescription] = useState('')

    const handleDescriptionChange = (event) => {
        const value = event.target.value
        setDescription(value)
    }

    const [location, setLocation] = useState('')

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const [locations, setLocations] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);

        }
    }

    useEffect(() => {
        fetchData();
        }, []);




return (
    <div className="row">
          <div className="col-md-6 offset-md-3 mt-5">
            <div className="card">
              <div className="card-header">
                <h3 className="text-center">Create Conference</h3>
              </div>
              <div className="card-body">
                <form onSubmit={handleSubmit} id="create-conference-form">
                  <div className="form-group">
                    <label htmlFor="name">Name</label>
                    <input onChange={handleNameChange} value={name} type="text" className="form-control" id="name" name="name" required />
                  </div>
                  <div className="form-group">
                    <label htmlFor="start-date">Start Date</label>
                    <input onChange={handleStartDateChange} value={startDate} type="date" className="form-control" id="start-date" name="starts" required />
                  </div>
                  <div className="form-group">
                    <label htmlFor="end-date">End Date</label>
                    <input onChange={handleEndDateChange} value={endDate} type="date" className="form-control" id="end-date" name="ends" required />
                  </div>
                  <div className="form-group">
                    <label htmlFor="max-presentations">Maximum Presentations</label>
                    <input onChange={handlePresentationsChange} value={maxPresentations} type="number" className="form-control" id="max-presentations" name="max_presentations" required />
                  </div>
                  <div className="form-group">
                    <label htmlFor="max-attendees">Maximum Attendees</label>
                    <input onChange={handleAttendeesChange} value={maxAttendees} type="number" className="form-control" id="max-attendees" name="max_attendees" required />
                  </div>
                  <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" name="description" rows="3" required></textarea>
                  </div>
                  <div className="form-group">
                    <select onChange={handleLocationChange} value={location.id} required id="location" name="location" className="form-control">
                      <option value="">Choose a location</option>
                      {locations.map(location => {
                      return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                      );
                      })}
                    </select>
                  </div>
                  <button type="submit" className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
        </div>
    </div>
  );
}
export default ConferenceForm;