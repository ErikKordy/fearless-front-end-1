// Add an event listener for when the DOM loads
window.addEventListener('DOMContentLoaded', async () => {
    // Declare a variable that will hold the URL for the API that was created
    const url = 'http://localhost:8000/api/locations/';

    // Fetch the URL. Don't forget the await keyword so that we get the response, not the Promise
    const response = await fetch(url);

    // If the response is ok, get the data using the .json method
    // Don't forget the await keyword
        if (response.ok) {
        const data = await response.json();

        const locationTag = document.getElementById("location");
            
        for (let location of data.locations) {

            const option = document.createElement('option');

            option.innerHTML = location.name

            option.value = location.id

            locationTag.appendChild(option)
        }
    }
});

// Add event listener for form submission
const formTag = document.getElementById('create-conference-form');
formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const conferencesUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(conferencesUrl, fetchConfig);
    if (response.ok) {
        formTag.reset();
        const newConferences = await response.json();
        console.log(newConferences)
    }
});
